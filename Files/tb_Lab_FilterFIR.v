`timescale 10ns/1ps

module tb_Lab_FilterFIR ();

parameter N_PROBE   = 8;
 
  reg             tb_clock = 1'b1;
  reg             tb_reset;
  wire  [N_PROBE - 1 : 0] tb_o_filtered_signal;

  wire [N_PROBE  - 1 : 0] w_signal;
  wire [N_PROBE - 1 : 0] w_probe0;
  wire [N_PROBE - 1 : 0] w_probe1;
  
  reg             aux_tb_reset;
  reg  [N_PROBE-1:0] aux_tb_is_data;

  //! Instance of FIR
  top_design
    #(
      .N_PROBE   (N_PROBE) 
    )
    u_top_design
      (
        .o_filtered_signal(tb_o_filtered_signal),
        .i_reset(tb_reset),
        .clock(tb_clock)
      );

  // Clock
  always #20 tb_clock = ~tb_clock;

 always @(posedge tb_clock)
  begin
    tb_reset    <= aux_tb_reset;
    //tb_o_filtered_signal <= aux_tb_is_data;
  end

  // Stimulus
  real i;
  real aux;
  initial begin
    $display("");
    $display("Simulation Started");
    //$dumpfile("./verification/tb_filtro_fir/waves.vcd");
    //$dumpvars(0, tb_filtro_fir);
    #5
    aux_tb_reset       = 1'b0;
    #40;
    aux_tb_reset       = 1'b1;
    #1000
    for (i=0;i<4000;i=i+1) begin
      aux = $sin(2.0*3.1415926*i/25000.0*1000.0)*(2**N_PROBE);
       if(aux > 127.0)
	 aux_tb_is_data = 8'h7F;
       else
	 aux_tb_is_data = aux;
      #40;
    end
    $display("Simulation Finished");
    $display("");
    $finish;
  end

endmodule
