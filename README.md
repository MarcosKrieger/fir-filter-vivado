# FIR Filter Vivado

Filtro de respuesta finita al impulso (FIR) en Vivado 2020.2 

Trabajo realizado durante el curso de EAMTA 2023 en el cual se lleva a cabo la implementación de forma remota, simulación y un análisis de reportes de síntesis y timming para distintas frecuencias de corte y de trabajo.

- La señal senoidal a filtrar estará compuestas por dos frecuencias $f_1=17kHz$ y $f_2=1.5kHz$.

- La frecuencia de muestreo será $f_s=48kHz$.

- El filtro será del tipo Pasa bajo y se modificarán su frecuencia de corte en valores de:

    
    - [ ] 6Khz   
    - [ ] 8Khz
    - [ ] 18Khz

----------------------------------------------------------------------------------

Finite Impulse Response (FIR) Filter in Vivado 2020.2

Work carried out during the EAMTA 2023 course in which the implementation is carried out remotely, simulation and an analysis of synthesis and timing reports for different cutting and work frequencies.

- The sinusoidal signal to be filtered will be composed of two frequencies $f_1=17Khz$ and $f_2=1.5Khz$.

- The sample rate will be $f_s=48Khz$.

- The filter will be of the Low Pass type and its cutoff frequency will be modified in values of:

    - [ ] 6Khz   
    - [ ] 8Khz
    - [ ] 18Khz

